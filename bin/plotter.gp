#!/usr/bin/gnuplot

system("mkdir -p plots")
file_list = system('ls data')
set terminal png size 1000, 400

do for [file in file_list] {
    data_file = sprintf("data/%s", file)
    plot_file = system("echo ".sprintf("plots/%s.png | cut -d '.' -f1,3", file))
    set title sprintf("%s", file)
    set output plot_file
    plot data_file using 1:2 with lines
}