#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <gsl/gsl_statistics_double.h>
#include "scats.h"

void read_series_from_file(char *file_name, int *series_size, double **times, double **series)
{
    FILE *file;
    file = fopen(file_name, "r");
    fscanf(file, "# %i", series_size);
    *times = malloc((*series_size) * sizeof(double));
    *series = malloc((*series_size) * sizeof(double));
    for (int i = 0; i < *series_size; i++)
        fscanf(file, "%lf %lf", &(*times)[i], &(*series)[i]);
    fclose(file);
}

void print_series_in_file(char *file_name, int series_size, double *times, double *series)
{
    FILE *file;
    file = fopen(file_name, "w");
    fprintf(file, "# %i \n", series_size);
    for (int i = 0; i < series_size; i++)
        fprintf(file, "%.15e %.15e \n", times[i], series[i]);
    fclose(file);
}

void go_next_line(FILE *file, int count)
{
}

void read_params(char *file_name, double *q, double *x)
{
    FILE *file;
    file = fopen(file_name, "r");
    char tmp;
    int tmp_int;
    double tmp_double;
    do
        tmp = fgetc(file);
    while (tmp != '\n');
    fscanf(file, "%lf %i %lf %lf", &tmp_double, &tmp_int, q, x);
    fclose(file);
}

int main(int argc, char *argv[])
{
    int series_size;
    double *times, *series;
    bool is_printed = true;
    read_series_from_file("./data/series.dat", &series_size, &times, &series);

    double *centered_series = exclude_trend(series_size, times, series, is_printed);
    print_series_in_file("./data/centered.dat", series_size, times, centered_series);

    int complex_series_size, periodogram_size;
    double *complex_series, *frequencies, *periodogram;
    double time_step = times[1] - times[0];
    get_periodogram(series_size, time_step, centered_series,
                    &complex_series_size, &complex_series,
                    &periodogram_size, &frequencies, &periodogram);
    print_series_in_file("./data/periodogram.dat", periodogram_size, frequencies, periodogram);

    double probability, critical_value;
    double series_var = gsl_stats_variance(centered_series, 1, series_size);
    printf("series var: %lf\n", series_var);
    read_params("./input.dat", &probability, &critical_value);
    double detection_threshold = series_var * critical_value / series_size;
    FILE *file;
    file = fopen("./threshold_info.dat", "w");
    fprintf(file, "%.15e %.15e \n", frequencies[0], detection_threshold);
    fprintf(file, "%.15e %.15e \n", frequencies[periodogram_size-1], detection_threshold);
    fclose(file);

    double *correlogram = get_correlogram(series_size, complex_series_size, complex_series);
    print_series_in_file("./data/correlogram.dat", series_size, times, correlogram);
    printf("correlogram[0]: %lf\n", correlogram[0]);

    double a = 0.25;
    int smoothed_periodogram_size1 = series_size / 4;
    int smoothed_periodogram_size2 = series_size / 10;
    double *smoothed_periodogram1 = get_smoothed_periodogram(a, smoothed_periodogram_size1,
                                                             complex_series_size,
                                                             correlogram, periodogram_size);
    double *smoothed_periodogram2 = get_smoothed_periodogram(a, smoothed_periodogram_size2,
                                                             complex_series_size,
                                                             correlogram, periodogram_size);
    print_series_in_file("./data/smt_periodogram1.dat", periodogram_size, frequencies, smoothed_periodogram1);
    print_series_in_file("./data/smt_periodogram2.dat", periodogram_size, frequencies, smoothed_periodogram2);
    return 0;
}
