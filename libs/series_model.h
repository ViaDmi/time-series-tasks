#ifndef SERIES_MODEL_H
#define SERIES_MODEL_H

struct series_model
{
    int steps_count;
    double t_step;

    double amplitude;
    double frequency;
    double phase;

    double SNR; // signal-to-noise ratio
    double lin_trend_a;
    double lin_trend_b;
} typedef series_model;

double get_series_value(double time, series_model *model);
void get_series(series_model *model, double **times, double **series);

#endif /* SERIES_MODEL_H */