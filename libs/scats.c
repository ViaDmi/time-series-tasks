#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_fft_complex.h>

double *exclude_trend(int series_size, double *times, double *series, bool is_printed)
{
    double c0, c1, cov00, cov01, cov11, sum_sq;
    gsl_fit_linear(times, 1, series, 1, series_size, &c0, &c1, &cov00, &cov01, &cov11, &sum_sq);
    if (is_printed)
    {
        printf("a: %f +- %f \n", c0, sqrt(cov00));
        printf("b: %f +- %f \n", c1, sqrt(cov11));
    }

    double *centered_series = malloc(sizeof(double) * series_size);
    for (int i = 0; i < series_size; i++)
        centered_series[i] = series[i] - c0 - c1 * times[i];
    return centered_series;
}

int _get_radix2_size(int size)
{
    int radix2_size = 1;
    do
        radix2_size *= 2;
    while (radix2_size < size);
    return radix2_size;
}

double *_transform_to_complex(int series_size, double *series, int complex_series_size)
{
    double *complex_series = malloc(2 * complex_series_size * sizeof(double));
    for (int i = 0; i < series_size; i++)
    {
        complex_series[2 * i] = series[i];
        complex_series[2 * i + 1] = 0.0;
    }
    for (int i = 2 * series_size; i < 2 * complex_series_size; i++)
        complex_series[i] = 0.0;
    return complex_series;
}

void get_periodogram(int series_size, double time_step, double *series,
                     int *complex_series_size, double **complex_series,
                     int *periodogram_size, double **frequencies, double **periodogram)
{
    *complex_series_size = 4 * _get_radix2_size(series_size);
    *complex_series = _transform_to_complex(series_size, series, *complex_series_size);
    gsl_fft_complex_radix2_forward(*complex_series, 1, (*complex_series_size));

    *periodogram_size = (*complex_series_size) / 2;
    double frequency_step = 1.0 / (time_step * (*complex_series_size));

    *frequencies = malloc((*complex_series_size) * sizeof(double));
    *periodogram = malloc((*complex_series_size) * sizeof(double));
    for (int i = 0; i < *periodogram_size; i++)
    {
        (*frequencies)[i] = i * frequency_step;
        (*periodogram)[i] = pow((*complex_series)[2 * i], 2) + pow((*complex_series)[2 * i + 1], 2);
        (*periodogram)[i] /= pow(series_size, 2);
    }
}

double *get_correlogram(int series_size, int complex_series_size, double *complex_series)
{
    for (int i = 0; i < complex_series_size; i++)
    {
        complex_series[2 * i] = pow(complex_series[2 * i], 2) + pow(complex_series[2 * i + 1], 2);
        complex_series[2 * i + 1] = 0.0;
    }
    gsl_fft_complex_radix2_inverse(complex_series, 1, complex_series_size);

    double *correlogram = malloc(complex_series_size * sizeof(double));
    for (int i = 0; i < complex_series_size; i++)
        correlogram[i] = complex_series[2 * i] / series_size;

    return correlogram;
}

double _tukey_weight(int m, int size, double a)
{
    return 1 - 2 * a + 2 * a * cos(M_PI * m / size);
}

double *get_weighted_correlogram(int weighted_correlogram_size, double a, double *correlogram)
{
    double *weighted_correlogram = malloc(weighted_correlogram_size * sizeof(double));
    for (int i = 0; i < weighted_correlogram_size; i++)
        weighted_correlogram[i] = _tukey_weight(i, weighted_correlogram_size, a) * correlogram[i];
    return weighted_correlogram;
}

double *get_smoothed_periodogram(double a, int smoothed_periodogram_size,
                                 int complex_series_size, double *correlogram, int periodogram_size)
{
    double *weighted_correlogram = get_weighted_correlogram(smoothed_periodogram_size, a, correlogram);
    double *complex_series = _transform_to_complex(smoothed_periodogram_size,
                                                   weighted_correlogram,
                                                   complex_series_size);
    gsl_fft_complex_radix2_forward(complex_series, 1, complex_series_size);

    double *smoothed_periodogram = malloc(periodogram_size * sizeof(double));
    for (int i = 0; i < periodogram_size; i++)
    {
        smoothed_periodogram[i] = (2 * complex_series[2 * i] - weighted_correlogram[0]);
        smoothed_periodogram[i] /= smoothed_periodogram_size;
    }
    free(complex_series);
    free(weighted_correlogram);
    return smoothed_periodogram;
}