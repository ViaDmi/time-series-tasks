COMPILER := gcc -Wall
LFLAGS := -lm -lgsl -lgslcblas

NAME0 := series_generator
NAME1 := scats-main
NAME2 := correlogram

TARGET0 := bin/$(NAME0).out
TARGET1 := bin/$(NAME1).out
TARGET2 := bin/$(NAME2).out

SRC := $(wildcard libs/*.c)
OBJ := $(patsubst libs/%.c, build/%.o, $(SRC))

INC := -I libs

all: $(TARGET0) $(TARGET1) $(TARGET2)

$(TARGET0): $(OBJ) build/$(NAME0).o
	$(COMPILER) $^ -o $@ $(LFLAGS)

$(TARGET1): $(OBJ) build/$(NAME1).o
	$(COMPILER) $^ -o $@ $(LFLAGS)

$(TARGET2): $(OBJ) build/$(NAME2).o
	$(COMPILER) $^ -o $@ $(LFLAGS)

build/$(NAME0): $(OBJ)
build/$(NAME1): $(OBJ)
build/$(NAME2): $(OBJ)

build/%.o: libs/%.c
	@mkdir -p build
	$(COMPILER) -c -o $@ $< $(INC)

build/%.o: src/%.c
	@mkdir -p build
	$(COMPILER) -c -o $@ $< $(INC)

get_series: $(TARGET0)
	(cd ./bin  && mkdir -p data && ./$(notdir $<))

exe_scats: $(TARGET1) bin/data/series.dat
	echo "SCATS running..."
	(cd ./bin && ./$(notdir $<))
	(cd bin && gnuplot plotter.gp)

exe_correlogram: $(TARGET2)
	echo "Correlogram program running..."
#	(cd ./bin && bash run-corr.sh) 
	(cd ./bin && ./$(notdir $<))

bin/data/series.dat:
	make get_series

bin/data:
	make exe_scats

report: bin/data
	(cd doc && make)

clean:
	rm -rf build bin/*.out bin/data bin/plots bin/threshold_info.dat 
	echo "Cleaned!"

doc_clean:
	(cd doc && make clean)

.PHONY: clean exe_scats get_series all report doc_clean exe_correlogram
.SILENT: clean exe_scats get_series exe_correlogram